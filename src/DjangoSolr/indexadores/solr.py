from django.conf import settings
import pysolr

solr_django = pysolr.Solr(settings.SOLR_ADDRESS)
