from .solr import solr_django
from DjangoSolr.collections.dado import Dado
from DjangoSolr.schemas import SolrMovimentacao, SolrMovimentacaoSchema
from typing import List, Dict
from bson import ObjectId
import numpy as np

TAMANHO_LOTE = 500
NOME_INDEXADOR = 'Solr'
VERSAO_ESQUEMA = 2


def get_projecao() -> Dict:

    return {
        '_id': 1,
        'ProcessoId': 1,
        'Detalhamento': 1,
        'DataDiario': 1,
        'DiarioId': 1,
        'Conteudo': 1
    }


def get_dados_nao_indexados(limite) -> List[Dict]:
    dado = Dado.get_dado_nao_indexado(get_projecao())

    if limite:
        dado = dado.limit(limite)

    return list(dado)


def indexar_com_solr(limite: int = None):
    dados = get_dados_nao_indexados(limite)

    if not dados:
        return

    indexar_lotes(dados)


def indexar_lote(lote: List[Dict]) -> None:
    documentos = []
    indexados: List[ObjectId] = []

    # -------------- Serialização
    for pub in lote:
        try:
            info = {
                'id': str(pub['_id']),
                'processo_id': str(pub['ProcessoId']),
                'movimentacao_id': str(pub['_id']),
                'movimentacao_tipo': 'publicacao',
                'diario_codigo': pub['DiarioId'],
                'conteudo': pub['Conteudo'],
                'data': str(pub['DataDiario']),
                'versao_esquema': '2',
                'detalhamento': pub['Detalhamento'],
            }

            print(info['data'])

            solr_movimentacao: \
                SolrMovimentacao = SolrMovimentacaoSchema().load(info)

            documentos.append(solr_movimentacao.__dict__)
            indexados.append(pub['_id'])
        except Exception as err:
            print(err)
            msg_err = 'Falha no parser da publicação [%s]: %s' % (
                pub['_id'],
                err,
            )

            continue

    # ------------- Indexação
    try:
        # print('start: ' + str(solr_movimentacao))
        solr_django.add(documentos, commit=True)
    except Exception as err:
        msg_err = 'Falha durante inclusao do documento no solr: %s' % err
        return

    # ------------- Atualização da Publicação
    for pub in lote:
        try:
            Dado.marcar_indexacao(
                _id=pub['_id'],
                indexador=NOME_INDEXADOR,
                versao_esquema=VERSAO_ESQUEMA,
            )
        except Exception as err:
            msg_err = 'Falha no update da publicação: %s' % err
            return


def indexar_lotes(dados: List[Dict]) -> None:
    dados_len = len(dados)
    lotes = np.array_split(
        dados,
        dados_len
    )

    for lote in lotes:
        indexar_lote(lote)
