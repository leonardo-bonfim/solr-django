from DjangoSolr.indexadores.indexar_solr import indexar_com_solr
from django.core.management import BaseCommand


class Command(BaseCommand):

    def add_arguments(self, parser) -> None:
        parser.add_argument(
            'limite',
            type=int,
            help='Número máximo de publicações que serão importadas.',
            default=5000,
            nargs='?',
        )

    def handle(self, *args, **options) -> None:
        self.stdout.write(self.style.SUCCESS(
            'Indexação com Solr iniciada. \n'
            'Pressione ctrl + C para interromper o processo.'
        ))

        while True:
            indexar_com_solr(limite=options.get('limite', None))
