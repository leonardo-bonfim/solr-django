from django.core.management import BaseCommand
from DjangoSolr.indexadores import solr


class Command(BaseCommand):

    def handle(self, *args, **options):
        a = solr.solr_django.search(
            '*:*'
        )

        print(a.docs)
