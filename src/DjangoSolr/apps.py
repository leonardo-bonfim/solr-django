from django.apps import AppConfig


class DjangosolrConfig(AppConfig):
    name = 'DjangoSolr'
