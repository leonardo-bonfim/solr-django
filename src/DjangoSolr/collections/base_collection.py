from typing import Any
from pymongo import MongoClient
from pymongo.collection import Collection
from pymongo.database import Database
from bson import ObjectId
from django.conf import settings

# Validação de ambiente
if not getattr(settings, 'MONGO_ADDRESS', None):
    raise KeyError(
        'Endereço do MongoDB não configurado. Defina um valor para a variável'
        ' <MONGO_ADDRESS> em settings.py'
    )

if not getattr(settings, 'MONGO_DB', None):
    raise KeyError(
        'Nome do banco não configurado. Defina uma valor para a variável'
        '<MONGO_DB> em settings.py'
    )

__all__ = [
    'BaseCollection'
]


class Meta(type):
    colecao_nome = None
    db: Database

    def __init__(cls, *args, **kwargs):
        cls.validar_collection(args)
        cls.colecao: Collection = cls.criar_colecao()
        if hasattr(cls, 'indices'):
            cls.colecao.create_indexes(cls.indices)

        super().__init__(*args, **kwargs)

    def validar_collection(cls, args: tuple) -> None:
        if args[0] != 'colecaoBase' and not args[2].get('colecao_nome'):
            raise ValueError(
                'Classe %s precisa definir uma <colecao_nome>' % args[2]
            )

    def criar_colecao(cls) -> Collection:
        """
        Retorna a instância da coleção referente à classe definida.

        :return:
        """
        cls.db = MongoClient(
            settings.MONGO_ADDRESS,
            retryWrites=False,
        )[settings.MONGO_DB]

        return cls.db[cls.colecao_nome]


class BaseCollection(metaclass=Meta):
    colecao_nome = 'colecaoBase'

    @classmethod
    def by_id(cls, _id: Any) -> dict:
        """
        Método helper para retornar um documento a partir de sua _id.

        :param _id: _id do documento no MongoDB
        :return:
        """
        if isinstance(_id, str):
            _id = ObjectId(_id)

        return cls.colecao.find_one({'_id': _id})

    @classmethod
    def delete_by_id(cls, _id: [str, ObjectId]) -> int:
        """
        Métoddo helper para realizar a exclusão de um documento com base eu seu
        _id.

        :param _id: _id do documento no MongDB
        :return:
        """
        if isinstance(_id, str):
            _id = ObjectId(_id)

        return cls.colecao.delete_one({'_id': _id}).deleted_count
