from typing import List
from datetime import datetime
from pymongo import IndexModel, ASCENDING
from pymongo.cursor import Cursor
from bson import ObjectId
from .base_collection import BaseCollection


class Dado(BaseCollection):
    colecao_nome = 'dado'
    indices = [
        IndexModel([('_id', ASCENDING)]),
        IndexModel([('Conteudo', ASCENDING)]),
        IndexModel(
            [('Solr.Indexado', -1)],
            background=True,
        ),
        IndexModel(
            [('Solr.VersaoEsquema', -1)],
            background=True,
        ),
    ]

    @classmethod
    def get_dado_nao_indexado(cls, projecao: dict = None) -> Cursor:

        filtro = {
            'Solr.Indexado': {
                '$in': [False, None]
            }
        }

        return cls.colecao.find(filtro, projecao)

    @classmethod
    def marcar_indexacao(cls, _id: ObjectId, indexador: str, versao_esquema: int) -> int:

        filtro = {
            '_id': _id
        }
        update = {
            '$set': {
                indexador:
                    {
                        'DataIndexacao': datetime.now(),
                        'Indexado': True,
                        'VersaoEsquema': versao_esquema,
                    }
            }
        }

        return cls.colecao.update_one(filtro, update).modified_count

    @classmethod
    def get_publicacoes_nao_indexadas(cls, projecao: dict = None) -> Cursor:
        """
        Retorna um cursor com as publicações que não foram indexadas pelo Solr.
        Opcionalmente recebe os valores que devem ser retornados.

        :param projecao: Dicionário com os valores que devem ser retornados.
        :return:
        """
        filtro = {
            'Solr.Indexado': {
                '$in': [False, None]
            }
        }

        return cls.colecao.find(filtro, projecao)
