from typing import *
from datetime import datetime
from dataclasses import dataclass
from marshmallow import fields, Schema, ValidationError, pre_load, post_load


@dataclass
class SolrMovimentacao:
    id: str
    processo_id: str
    movimentacao_id: str
    movimentacao_tipo: str
    diario_codigo: int
    conteudo: str
    data: datetime
    detalhamento: str
    versao_esquema: int


class SolrMovimentacaoSchema(Schema):
    class Meta:
        format: str = '%Y-%m-%d %H:%M:%S'

    modelo = SolrMovimentacao
    id = fields.Str(required=False)
    processo_id = fields.Str(required=True)
    movimentacao_id = fields.Str(required=True)
    movimentacao_tipo = fields.Str(required=True)
    diario_codigo = fields.Int(required=True)
    conteudo = fields.Str(required=True)
    data = fields.DateTime(required=True)
    detalhamento = fields.Str(required=True)
    versao_esquema = fields.Int(required=False)

    @post_load
    def _post_load(self, data, **kwargs) -> 'SolrMovimentacao':
        return SolrMovimentacao(**data)
