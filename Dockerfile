FROM python:3

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code

COPY ./src/requirements.txt /code
RUN pip install -r requirements.txt
COPY ./src/ /code/
